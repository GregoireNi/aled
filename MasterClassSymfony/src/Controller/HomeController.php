<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
//use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
//use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;
use App\Entity\Post;
//use App\Repository\PostRepository;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class HomeController extends AbstractController
{
    public function showHome()
    {
        return $this->render('front/home.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }

    public function showOnePost(Post $post = null)
    {
        if (!$post) {
            throw $this->createNotFoundException(
                'No product found for id '.$id
            );
        }
        return $this->render('front/showOnePost.html.twig',[
            'post' => $post
        ]);
    }

    public function showDashBoard()
    {
        return $this->render('bo/dashBoard.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }

    //géré par Sécurity
    public function logout()
    {

    }

    //géré par Sécurity
    public function login(AuthenticationUtils $authenticationUtils)
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        $user = new User();
        $user->setUsername('ryan');
        $user->setPassword('ryanpass');

        $form = $this->createFormBuilder($user)
            ->add('_username', TextType::class)
            ->add('_password', TextType::class)
            ->add('save', SubmitType::class, ['label' => 'Se connecter'])
            ->getForm();

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            //$this->redirectToRoute('task_success');
        }

        return $this->render('front/logForm.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error,
            'form' => $form->createView(),
        ]);
    }

    public function createPost()
    {
        $entityManager = $this->getDoctrine()->getManager();

        $post = new Post();
        $post->settitle('ALED');
        $post->setDescription('BDL3 DEMAIIIIINN!!!');

        $entityManager->persist($post);

        $entityManager->flush();

       return $this->redirectToRoute("home");
    }

}
